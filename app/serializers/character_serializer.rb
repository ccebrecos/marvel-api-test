class CharacterSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :thumbnail, :resourceURI, :status

  has_many :comics
end
