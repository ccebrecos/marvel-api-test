class ComicsController < ApplicationController
  before_action :set_character

  def index
    json_response(@character.comics)
  end

  private

  def comic_params
    params.permit(:title, :resourceURI)
  end

  def set_character
    @character = Character.find(params[:character_id])
  end

  def set_comic_character
    @comic = @character.comics.find_by!(id: params[:id]) if @character
  end

end
