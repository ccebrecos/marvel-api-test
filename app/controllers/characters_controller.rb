class CharactersController < ApplicationController
  before_action :set_character, only: %i[show update destroy]

  def index
    @characters = current_user.characters
    json_response(@characters)
  end

  def create
    @character = current_user.characters.create!(character_params)
    fetched_data = MarvelService.fetch_character_data @character.name
    @character.update(fetched_data)
    json_response(@character, :created)
  end

  def show
    json_response(@character)
  end

  def update
    @character.update(character_params)
    head :no_content
  end

  def destroy
    @character.destroy
    head :no_content
  end

  private

  def character_params
    params.permit(:name, :description, :thumbnail, :status, comics_attributes: %i[title resourceURI])
  end

  def set_character
    @character = current_user.characters.find(params[:id])
  end
end
