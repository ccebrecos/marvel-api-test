require 'digest'
require 'rest-client'
require 'json'

class MarvelService

  API_URL = 'https://gateway.marvel.com:443/v1/public'.freeze
  @ts = 0

  @secret_key = '05a0344d2bd18e52d98eb17aadc61eeebf791937'
  @public_key = '731d367922e3a398cc2bb857bfb0fc66'

  def self.fetch_character_data(character_name)
    uri = "#{API_URL}/characters?name=#{CGI.escape character_name}&apikey=#{@public_key}&ts=#{@ts}&hash=#{compute_hash}"
    response = RestClient.get uri

    if response.code == 200
      json_response = JSON.parse response.to_str

      if json_response['data']['count'] > 0
        description = json_response['data']['results'][0]['description']
        thumbnail = json_response['data']['results'][0]['thumbnail']['path']
        resourceURI = json_response['data']['results'][0]['resourceURI']
        comics = json_response['data']['results'][0]['comics']['items']

        {
          'status': 'FOUND',
          'description': description,
          'thumbnail': thumbnail,
          'resourceURI': resourceURI,
          'comics': comics.map { |comic| Comic.new(title: comic['name'], resourceURI: comic['resourceURI']) }
        }
      else
        {
          'status': 'NOT FOUND'
        }
      end
    end
  end

  def self.compute_hash
    dig = Digest::MD5.hexdigest "#{@ts}#{@secret_key}#{@public_key}"
    @ts += 1
    dig
  end

end
