class Character < ApplicationRecord
  has_many :comics, dependent: :destroy, autosave: true

  accepts_nested_attributes_for :comics
  validates_presence_of :name
end
