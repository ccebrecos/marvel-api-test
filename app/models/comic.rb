class Comic < ApplicationRecord
  belongs_to :character

  validates_presence_of :title, :resourceURI
end
