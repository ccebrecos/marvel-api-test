require 'rails_helper'

RSpec.describe Comic, type: :model do
  it { should belong_to(:character) }

  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:resourceURI) }
end
