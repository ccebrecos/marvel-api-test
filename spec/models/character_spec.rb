require 'rails_helper'

RSpec.describe Character, type: :model do
  it { should have_many(:comics).dependent(:destroy) }

  it { should validate_presence_of(:name) }
end
