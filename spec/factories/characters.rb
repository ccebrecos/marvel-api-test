FactoryBot.define do
  factory :character do
    name { Faker::Lorem.word }
  end
end
