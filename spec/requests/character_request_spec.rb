require 'rails_helper'

RSpec.describe 'Characters API', type: :request do

  let(:user) { create(:user) }
  let!(:characters) { create_list(:character, 10, created_by: user.id) }
  let(:character_id) { characters.first.id }

  let(:headers) { valid_headers }

  describe 'GET /characters' do
    before { get '/characters', params: {}, headers: headers }

    it 'returns characters' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /characters/:id' do
    before { get "/characters/#{character_id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the character' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(character_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status 200
      end
    end

    context 'when the record does not exist' do
      let(:character_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status 404
      end

      it 'returns a not found message' do
        expect(response.body).to match("Couldn't find Character")
      end
    end
  end

  describe 'POST /characters' do
    let(:valid_character) do
      { name: 'Iron Man', created_by: user.id.to_s }.to_json
    end

    context 'when the request is valid' do
      before { post '/characters', params: valid_character, headers: headers }
      it 'creates a character' do
        expect(json['name']).to eq('Iron Man')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status 201
      end
    end

    # context 'when the request is invalid' do
    #   before { post '/characters', params: { abc: 'Iron Man'} }
    #
    #   it 'returns status code 422' do
    #     expect(response).to have_http_status 422
    #   end
    # end

  end
end
