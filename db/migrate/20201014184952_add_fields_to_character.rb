class AddFieldsToCharacter < ActiveRecord::Migration[6.0]
  def change
    add_column(:characters, :description, :string)
    add_column(:characters, :thumbnail, :string)
    add_column(:characters, :resourceURI, :string)
    add_column(:characters, :status, :string, default: 'SEARCHING')
  end
end
