## Mejoras para la solución propuesta:

- Validación de los atributos de los modelos en su creación
- Mayor coverage de los tests
- Ubicación de los ficheros
- Arquitectura propuesta
- Consumir la API de marvel en paralelo sin necesidad de quedar bloqueado esperando respuesta
- Credenciales para conexión de la API de marvel en variables de entorno
